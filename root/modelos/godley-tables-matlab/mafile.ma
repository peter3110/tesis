#include(macros.inc)

%Coupled model
[top]
components: BankShare0@BankSharetop ConsB0@ConsBtop ConsW0@ConsWtop GDProduct0@GDProducttop Interest0@Interesttop LendF0@LendFtop Money0@Moneytop ProfitGross0@ProfitGrosstop ProfitNet0@ProfitNettop ProfitShare0@ProfitSharetop RepayF0@RepayFtop Wages0@Wagestop WagesShare0@WagesSharetop velocityOfMoney0@velocityOfMoneytop  DEVS_BASIC_COUPLED_Bank  DEVS_BASIC_COUPLED_DebtFirms  DEVS_BASIC_COUPLED_Firms  DEVS_BASIC_COUPLED_Workers rateOfInterestOnLoans0@rateOfInterestOnLoanstop share0@sharetop tauB0@tauBtop tauL0@tauLtop tauR0@tauRtop tauT0@tauTtop tauW0@tauWtop
% In ports
in: in_port_rateOfInterestOnLoans in_port_share in_port_tauB in_port_tauL in_port_tauR in_port_tauT in_port_tauW
% Out ports
out: out_port_Bank out_port_BankShare out_port_ConsB out_port_ConsW out_port_DebtFirms out_port_Firms out_port_GDProduct out_port_Interest out_port_LendF out_port_Money out_port_ProfitGross out_port_ProfitNet out_port_ProfitShare out_port_RepayF out_port_Wages out_port_WagesShare out_port_Workers out_port_chgBank out_port_chgDebtFirms out_port_chgFirms out_port_chgWorkers out_port_velocityOfMoney
% Input connections
link: in_port_rateOfInterestOnLoans in_port_rateOfInterestOnLoans@rateOfInterestOnLoans0
link: in_port_share in_port_share@share0
link: in_port_tauB in_port_tauB@tauB0
link: in_port_tauL in_port_tauL@tauL0
link: in_port_tauR in_port_tauR@tauR0
link: in_port_tauT in_port_tauT@tauT0
link: in_port_tauW in_port_tauW@tauW0
% Output connections
link: out_port_Bank@DEVS_BASIC_COUPLED_Bank out_port_Bank
link: out_port_BankShare@BankShare0 out_port_BankShare
link: out_port_ConsB@ConsB0 out_port_ConsB
link: out_port_ConsW@ConsW0 out_port_ConsW
link: out_port_DebtFirms@DEVS_BASIC_COUPLED_DebtFirms out_port_DebtFirms
link: out_port_Firms@DEVS_BASIC_COUPLED_Firms out_port_Firms
link: out_port_GDProduct@GDProduct0 out_port_GDProduct
link: out_port_Interest@Interest0 out_port_Interest
link: out_port_LendF@LendF0 out_port_LendF
link: out_port_Money@Money0 out_port_Money
link: out_port_ProfitGross@ProfitGross0 out_port_ProfitGross
link: out_port_ProfitNet@ProfitNet0 out_port_ProfitNet
link: out_port_ProfitShare@ProfitShare0 out_port_ProfitShare
link: out_port_RepayF@RepayF0 out_port_RepayF
link: out_port_Wages@Wages0 out_port_Wages
link: out_port_WagesShare@WagesShare0 out_port_WagesShare
link: out_port_Workers@DEVS_BASIC_COUPLED_Workers out_port_Workers
link: out_port_chgBank@DEVS_BASIC_COUPLED_Bank out_port_chgBank
link: out_port_chgDebtFirms@DEVS_BASIC_COUPLED_DebtFirms out_port_chgDebtFirms
link: out_port_chgFirms@DEVS_BASIC_COUPLED_Firms out_port_chgFirms
link: out_port_chgWorkers@DEVS_BASIC_COUPLED_Workers out_port_chgWorkers
link: out_port_velocityOfMoney@velocityOfMoney0 out_port_velocityOfMoney
% Internal connections
link: out_port_GDProduct@GDProduct0 in_port_GDProduct@BankShare0
link: out_port_Interest@Interest0 in_port_Interest@BankShare0
link: out_port_Bank@DEVS_BASIC_COUPLED_Bank in_port_Bank@ConsB0
link: out_port_tauB@tauB0 in_port_tauB@ConsB0
link: out_port_Workers@DEVS_BASIC_COUPLED_Workers in_port_Workers@ConsW0
link: out_port_tauW@tauW0 in_port_tauW@ConsW0
link: out_port_ConsB@ConsB0 in_port_ConsB@DEVS_BASIC_COUPLED_Bank
link: out_port_Interest@Interest0 in_port_Interest@DEVS_BASIC_COUPLED_Bank
link: out_port_LendF@LendF0 in_port_LendF@DEVS_BASIC_COUPLED_DebtFirms
link: out_port_RepayF@RepayF0 in_port_RepayF@DEVS_BASIC_COUPLED_DebtFirms
link: out_port_ConsB@ConsB0 in_port_ConsB@DEVS_BASIC_COUPLED_Firms
link: out_port_ConsW@ConsW0 in_port_ConsW@DEVS_BASIC_COUPLED_Firms
link: out_port_Interest@Interest0 in_port_Interest@DEVS_BASIC_COUPLED_Firms
link: out_port_LendF@LendF0 in_port_LendF@DEVS_BASIC_COUPLED_Firms
link: out_port_RepayF@RepayF0 in_port_RepayF@DEVS_BASIC_COUPLED_Firms
link: out_port_Wages@Wages0 in_port_Wages@DEVS_BASIC_COUPLED_Firms
link: out_port_ConsW@ConsW0 in_port_ConsW@DEVS_BASIC_COUPLED_Workers
link: out_port_Wages@Wages0 in_port_Wages@DEVS_BASIC_COUPLED_Workers
link: out_port_Firms@DEVS_BASIC_COUPLED_Firms in_port_Firms@GDProduct0
link: out_port_tauT@tauT0 in_port_tauT@GDProduct0
link: out_port_DebtFirms@DEVS_BASIC_COUPLED_DebtFirms in_port_DebtFirms@Interest0
link: out_port_rateOfInterestOnLoans@rateOfInterestOnLoans0 in_port_rateOfInterestOnLoans@Interest0
link: out_port_DebtFirms@DEVS_BASIC_COUPLED_DebtFirms in_port_DebtFirms@LendF0
link: out_port_tauL@tauL0 in_port_tauL@LendF0
link: out_port_Bank@DEVS_BASIC_COUPLED_Bank in_port_Bank@Money0
link: out_port_Firms@DEVS_BASIC_COUPLED_Firms in_port_Firms@Money0
link: out_port_Workers@DEVS_BASIC_COUPLED_Workers in_port_Workers@Money0
link: out_port_GDProduct@GDProduct0 in_port_GDProduct@ProfitGross0
link: out_port_Wages@Wages0 in_port_Wages@ProfitGross0
link: out_port_Interest@Interest0 in_port_Interest@ProfitNet0
link: out_port_ProfitGross@ProfitGross0 in_port_ProfitGross@ProfitNet0
link: out_port_GDProduct@GDProduct0 in_port_GDProduct@ProfitShare0
link: out_port_ProfitNet@ProfitNet0 in_port_ProfitNet@ProfitShare0
link: out_port_DebtFirms@DEVS_BASIC_COUPLED_DebtFirms in_port_DebtFirms@RepayF0
link: out_port_tauR@tauR0 in_port_tauR@RepayF0
link: out_port_GDProduct@GDProduct0 in_port_GDProduct@Wages0
link: out_port_share@share0 in_port_share@Wages0
link: out_port_GDProduct@GDProduct0 in_port_GDProduct@WagesShare0
link: out_port_Wages@Wages0 in_port_Wages@WagesShare0
link: out_port_GDProduct@GDProduct0 in_port_GDProduct@velocityOfMoney0
link: out_port_Money@Money0 in_port_Money@velocityOfMoney0
%Coupled model
[DEVS_BASIC_COUPLED_Bank]
components: chgBank_Bank1@chgBank_BankDEVS_BASIC_COUPLED_Bank TotBank1@TotBankDEVS_BASIC_COUPLED_Bank Bank1@BankDEVS_BASIC_COUPLED_Bank
% In ports
in: in_port_ConsB in_port_Interest
% Out ports
out: out_port_Bank out_port_chgBank
% Input connections
link: in_port_ConsB in_port_ConsB@chgBank_Bank1
link: in_port_Interest in_port_Interest@chgBank_Bank1
% Output connections
link: out_port_Bank@Bank1 out_port_Bank
link: out_port_chgBank@chgBank_Bank1 out_port_chgBank
% Internal connections
link: out_port_TotBank@TotBank1 in_port_TotBank@Bank1
link: out_port_chgBank_Bank@chgBank_Bank1 in_plus_port_chgBank_Bank@TotBank1
[Bank1]
% Atomic model DEVSIntegrator
x0: 8
non_negative: 0
dQMin: 0.0001
dQRel: 0.0001

%Coupled model
[DEVS_BASIC_COUPLED_DebtFirms]
components: chgDebtFirms_DebtFirms1@chgDebtFirms_DebtFirmsDEVS_BASIC_COUPLED_DebtFirms TotDebtFirms1@TotDebtFirmsDEVS_BASIC_COUPLED_DebtFirms DebtFirms1@DebtFirmsDEVS_BASIC_COUPLED_DebtFirms
% In ports
in: in_port_LendF in_port_RepayF
% Out ports
out: out_port_DebtFirms out_port_chgDebtFirms
% Input connections
link: in_port_LendF in_port_LendF@chgDebtFirms_DebtFirms1
link: in_port_RepayF in_port_RepayF@chgDebtFirms_DebtFirms1
% Output connections
link: out_port_DebtFirms@DebtFirms1 out_port_DebtFirms
link: out_port_chgDebtFirms@chgDebtFirms_DebtFirms1 out_port_chgDebtFirms
% Internal connections
link: out_port_TotDebtFirms@TotDebtFirms1 in_port_TotDebtFirms@DebtFirms1
link: out_port_chgDebtFirms_DebtFirms@chgDebtFirms_DebtFirms1 in_plus_port_chgDebtFirms_DebtFirms@TotDebtFirms1
[DebtFirms1]
% Atomic model DEVSIntegrator
x0: 100
non_negative: 0
dQMin: 0.0001
dQRel: 0.0001

%Coupled model
[DEVS_BASIC_COUPLED_Firms]
components: chgFirms_Firms1@chgFirms_FirmsDEVS_BASIC_COUPLED_Firms TotFirms1@TotFirmsDEVS_BASIC_COUPLED_Firms Firms1@FirmsDEVS_BASIC_COUPLED_Firms
% In ports
in: in_port_ConsB in_port_ConsW in_port_Interest in_port_LendF in_port_RepayF in_port_Wages
% Out ports
out: out_port_Firms out_port_chgFirms
% Input connections
link: in_port_ConsB in_port_ConsB@chgFirms_Firms1
link: in_port_ConsW in_port_ConsW@chgFirms_Firms1
link: in_port_Interest in_port_Interest@chgFirms_Firms1
link: in_port_LendF in_port_LendF@chgFirms_Firms1
link: in_port_RepayF in_port_RepayF@chgFirms_Firms1
link: in_port_Wages in_port_Wages@chgFirms_Firms1
% Output connections
link: out_port_Firms@Firms1 out_port_Firms
link: out_port_chgFirms@chgFirms_Firms1 out_port_chgFirms
% Internal connections
link: out_port_TotFirms@TotFirms1 in_port_TotFirms@Firms1
link: out_port_chgFirms_Firms@chgFirms_Firms1 in_plus_port_chgFirms_Firms@TotFirms1
[Firms1]
% Atomic model DEVSIntegrator
x0: 80
non_negative: 0
dQMin: 0.0001
dQRel: 0.0001

%Coupled model
[DEVS_BASIC_COUPLED_Workers]
components: chgWorkers_Workers1@chgWorkers_WorkersDEVS_BASIC_COUPLED_Workers TotWorkers1@TotWorkersDEVS_BASIC_COUPLED_Workers Workers1@WorkersDEVS_BASIC_COUPLED_Workers
% In ports
in: in_port_ConsW in_port_Wages
% Out ports
out: out_port_Workers out_port_chgWorkers
% Input connections
link: in_port_ConsW in_port_ConsW@chgWorkers_Workers1
link: in_port_Wages in_port_Wages@chgWorkers_Workers1
% Output connections
link: out_port_Workers@Workers1 out_port_Workers
link: out_port_chgWorkers@chgWorkers_Workers1 out_port_chgWorkers
% Internal connections
link: out_port_chgWorkers_Workers@chgWorkers_Workers1 in_plus_port_chgWorkers_Workers@TotWorkers1
link: out_port_TotWorkers@TotWorkers1 in_port_TotWorkers@Workers1
[Workers1]
% Atomic model DEVSIntegrator
x0: 12
non_negative: 0
dQMin: 0.0001
dQRel: 0.0001
