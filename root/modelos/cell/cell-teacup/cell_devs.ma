#include(macros.inc)

[cell]
% Parameters
dim: (5,5,2)
initialvalue: -70
type: cell
delay: transport
defaultDelayTime: 5
localtransition: opinion-rule
initialCellsValue: valfile.val
border: unwrapped

% Neighbors
neighbors: (0,-1,0) (-1,0,0) (0,0,1) (0,0,0) (0,1,0) (1,0,0)
% Zones

zone: pared-rule { (0,0,0)..(2,2,0) (1,1,1)..(2,2,2) }
%In ports
in: in1 in2 in3
% Out ports
out: out1 out2 out3
% Input connections
% Output connections
% Internal connections
link: in1 in@cell(0,0,0)
link: out@cell(0,0,0) out1
% Ports in transition
PortInTransition: in@cell(0,0,0) shock-rule
% Transitions
[pared-rule]
rule: { 100 } 5 { t }
[opinion-rule]
rule: { randInt(3)+1 } 5 { (0,0,1)=? }
rule: { -3 } 5 { (0,0,1)=1  and (0,0,0) < -1 and (((0,0,0) - #macro(delta))<-3 )and ( (0,1,0) < (0,0,0) or  (0,1,0) > 1) }
rule: { 3 } 5 { (0,0,1)=1  and (0,0,0) < -1 and (0,1,0) <= 1 and (((0,0,0) + #macro(delta))>3) }
rule: { 3 } 5 { (0,0,1)=1  and (0,0,0) > 1 and (((0,0,0) + #macro(delta))>3) and ((0,1,0) > (0,0,0)  or  (0,1,0) <= -1 ) }
rule: { -3 } 5 { (0,0,1)=1  and (0,0,0) > 1 and (((0,0,0) - #macro(delta))<-3) and (0,1,0) < (0,0,0) }
rule: { -3 } 5 { (0,0,1)=3  and (0,0,0) < -1 and (((0,0,0) - #macro(delta))<-3) and ( (0,-1,0) < (0,0,0) or  (0,-1,0) > 1) }
rule: { 3 } 5 { (0,0,1)=3  and (0,0,0) < -1 and (((0,0,0) + #macro(delta))>3) and (0,-1,0) <= 1 }
rule: { 3 } 5 { (0,0,1)=3  and (0,0,0) > 1 and (((0,0,0) + #macro(delta))>3) and ((0,-1,0) > (0,0,0)  or  (0,-1,0) <= -1 ) }
rule: { -3 } 5 { (0,0,1)=3  and (0,0,0) > 1 and (((0,0,0) - #macro(delta))<-3) and (0,-1,0) < (0,0,0) }
rule: { -3 } 5 { (0,0,1)=2  and (0,0,0) < -1 and (((0,0,0) - #macro(delta))<-3) and ( (-1,0,0) < (0,0,0) or  (-1,0,0) > 1) }
rule: { 3 } 5 { (0,0,1)=2  and (0,0,0) < -1 and (((0,0,0) + #macro(delta))>3) and (-1,0,0) <= 1 }
rule: { 3 } 5 { (0,0,1)=2  and (0,0,0) > 1 and (((0,0,0) + #macro(delta))>3) and ((-1,0,0) > (0,0,0)  or  (-1,0,0) <= -1 ) }
rule: { -3 } 5 { (0,0,1)=2  and (0,0,0) > 1 and (((0,0,0) - #macro(delta))<-3) and (-1,0,0) < (0,0,0)  }
rule: { -3 } 5 { (0,0,1)=4  and (0,0,0) < -1 and (((0,0,0) - #macro(delta))<-3) and ( (1,0,0) < (0,0,0) or  (1,0,0) > 1) }
rule: { 3 } 5 { (0,0,1)=4  and (0,0,0) < -1 and (((0,0,0) + #macro(delta))>3) and (1,0,0) <= 1 }
rule: { 3 } 5 { (0,0,1)=4  and (0,0,0) > 1 and (((0,0,0) + #macro(delta))>3) and ((1,0,0) > (0,0,0)  or  (1,0,0) <= -1 ) }
rule: { -3 } 5 { (0,0,1)=4  and (0,0,0) > 1 and (((0,0,0) - #macro(delta))<-3) and (1,0,0) < (0,0,0) }
rule: { (0,0,0) } 5 { (0,0,1)=1  and ((0,1,0)=? or (0,1,0)=(0,0,0)) }
rule: { (0,0,0) - #macro(delta) } 5 { (0,0,1)=1 and (0,0,0)<=-#macro(long) and (0,1,0)>=#macro(long) }
rule: { (0,0,0) + #macro(delta) } 5 { (0,0,1)=1 and (0,0,0)>=#macro(long) and (0,1,0)<=-#macro(long) }
rule: { (0,0,0) } 5 { (0,0,1)=1 and abs((0,0,0))>=abs(#macro(long)) and abs((0,1,0))>1 }
rule: { (0,0,0) - #macro(delta) } 5 { (0,0,1)=1 and (0,0,0)>1 and abs((0,1,0))<=1 }
rule: { (0,0,0) + #macro(delta) } 5 { (0,0,1)=1 and (0,0,0)<-1 and abs((0,1,0))<=1 }
rule: { (0,0,0) - #macro(delta) } 5 { (0,0,1)=1 and (0,0,0)<-1 and (0,0,0)>-#macro(long) and (0,1,0)<=-#macro(long) }
rule: { (0,0,0) + #macro(delta) } 5 { (0,0,1)=1 and (0,0,0)>1 and (0,0,0)<#macro(long) and (0,1,0)>=#macro(long) }
rule: { (0,0,0) + #macro(q)*#macro(delta) } 5 { (0,0,1)=1 and (0,0,0)<-1 and (0,0,0)>-#macro(long) and (0,1,0)>=#macro(long) }
rule: { (0,0,0) - #macro(q)*#macro(delta) } 5 { (0,0,1)=1 and (0,0,0)>1 and (0,0,0)<#macro(long) and (0,1,0)<=-#macro(long) }
rule: { (0,0,0) + #macro(k)*#macro(delta) } 5 { (0,0,1)=1 and abs((0,0,0))<=1 and (0,1,0)>1 and (0,1,0)<#macro(long) }
rule: { (0,0,0) - #macro(k)*#macro(delta) } 5 { (0,0,1)=1 and abs((0,0,0))<=1 and (0,1,0)<-1 and (0,1,0)>-#macro(long) }
rule: { (0,0,0) + #macro(q)*#macro(delta) } 5 { (0,0,1)=1 and abs((0,0,0))<=1 and (0,1,0)>=#macro(long) }
rule: { (0,0,0) - #macro(q)*#macro(delta) } 5 { (0,0,1)=1 and abs((0,0,0))<=1 and (0,1,0)<-#macro(long) }
rule: { (0,0,0)*0 } 5 { (0,0,1)=1 and abs((0,0,0))<=#macro(delta) and abs((0,1,0))<=1 }
rule: { (0,0,0) - #macro(delta) } 5 { (0,0,1)=1 and abs((0,0,0))<=1 and abs((0,1,0))<=1 and (0,0,0)>0 }
rule: { (0,0,0) + #macro(delta) } 5 { (0,0,1)=1 and abs((0,0,0))<=1 and abs((0,1,0))<=1 and (0,0,0)<0 }
rule: { (0,0,0) + #macro(delta) } 5 { (0,0,1)=1 and (0,0,0)>1 and (0,1,0)>1 and (0,0,0)<(0,1,0) }
rule: { (0,0,0) } 5 { (0,0,1)=1 and (0,0,0)>1 and (0,1,0)>1 and (0,0,0)>(0,1,0) }
rule: { (0,0,0) - #macro(delta) } 5 { (0,0,1)=1 and (0,0,0)<-1 and (0,1,0)<-1 and (0,0,0)>(0,1,0) }
rule: { (0,0,0) } 5 { (0,0,1)=1 and (0,0,0)<-1 and (0,1,0)<-1 and (0,0,0)<(0,1,0) }
rule: { if( randInt(1) < 1,(0,0,0) + #macro(delta),(0,0,0) - #macro(delta) )  } 5 { (0,0,1)=1 and (0,0,0)*(0,1,0)<=-1   }
rule: { (0,0,0) } 5 {  (0,0,1)=2  and ((1,0,0)=? or (1,0,0)=(0,0,0)) }
rule: { (0,0,0) - #macro(delta) } 5 {  (0,0,1)=2 and (0,0,0)<=-#macro(long) and (1,0,0)>=#macro(long)  }
rule: { (0,0,0) + #macro(delta) } 5 {  (0,0,1)=2 and (0,0,0)>=#macro(long) and (1,0,0)<=-#macro(long)  }
rule: { (0,0,0) } 5 {  (0,0,1)=2 and abs((0,0,0))>=abs(#macro(long)) and abs((1,0,0))>1  }
rule: { (0,0,0) - #macro(delta) } 5 {  (0,0,1)=2 and (0,0,0)>1 and abs((1,0,0))<=1  }
rule: { (0,0,0) + #macro(delta) } 5 {  (0,0,1)=2 and (0,0,0)<-1 and abs((1,0,0))<=1   }
rule: { (0,0,0) - #macro(delta) } 5 {  (0,0,1)=2 and (0,0,0)<-1 and (0,0,0)>-#macro(long) and (1,0,0)<=-#macro(long)  }
rule: { (0,0,0) + #macro(delta) } 5 {  (0,0,1)=2 and (0,0,0)>1 and (0,0,0)<#macro(long) and (1,0,0)>=#macro(long)  }
rule: { (0,0,0) + #macro(q)*#macro(delta) } 5 {  (0,0,1)=2 and (0,0,0)<-1 and (0,0,0)>-#macro(long) and (1,0,0)>=#macro(long)  }
rule: { (0,0,0) - #macro(q)*#macro(delta) } 5 {  (0,0,1)=2 and (0,0,0)>1 and (0,0,0)<#macro(long) and (1,0,0)<=-#macro(long)  }
rule: { (0,0,0) + #macro(k)*#macro(delta) } 5 {  (0,0,1)=2 and abs((0,0,0))<=1 and (1,0,0)>1 and (1,0,0)<#macro(long) }
rule: { (0,0,0) - #macro(k)*#macro(delta) } 5 {  (0,0,1)=2 and abs((0,0,0))<=1 and (1,0,0)<-1 and (1,0,0)>-#macro(long) }
rule: { (0,0,0) + #macro(q)*#macro(delta) } 5 {  (0,0,1)=2 and abs((0,0,0))<=1 and (1,0,0)>=#macro(long)  }
rule: { (0,0,0) - #macro(q)*#macro(delta) } 5 {  (0,0,1)=2 and abs((0,0,0))<=1 and (1,0,0)<-#macro(long)  }
rule: { (0,0,0)*0 } 5 {  (0,0,1)=2 and abs((0,0,0))<=#macro(delta) and abs((1,0,0))<=1  }
rule: { (0,0,0) - #macro(delta) } 5 {  (0,0,1)=2 and abs((0,0,0))<=1 and abs((1,0,0))<=1 and (0,0,0)>0  }
rule: { (0,0,0) + #macro(delta) } 5 {  (0,0,1)=2 and abs((0,0,0))<=1 and abs((1,0,0))<=1 and (0,0,0)<0  }
rule: { (0,0,0) + #macro(delta) } 5 {  (0,0,1)=2 and (0,0,0)>1 and (1,0,0)>1 and (0,0,0)<(1,0,0)  }
rule: { (0,0,0) } 5 {  (0,0,1)=2 and (0,0,0)>1 and (1,0,0)>1 and (0,0,0)>(1,0,0)  }
rule: { (0,0,0) - #macro(delta) } 5 {  (0,0,1)=2 and (0,0,0)<-1 and (1,0,0)<-1 and (0,0,0)>(1,0,0)  }
rule: { (0,0,0) } 5 {  (0,0,1)=2 and (0,0,0)<-1 and (1,0,0)<-1 and (0,0,0)<(1,0,0)  }
rule: { if( randInt(1) < 1,(0,0,0) + #macro(delta),(0,0,0) - #macro(delta) )  } 5 {  (0,0,1)=2 and (0,0,0)*(1,0,0)<=-1    }
rule: { (0,0,0)  } 5 {  (0,0,1)=3  and ((0,-1,0)=? or (0,-1,0)=(0,0,0)) }
rule: { (0,0,0) - #macro(delta)  } 5 {  (0,0,1)=3 and (0,0,0)<=-#macro(long) and (0,-1,0)>=#macro(long)  }
rule: { (0,0,0) + #macro(delta)  } 5 {  (0,0,1)=3 and (0,0,0)>=#macro(long) and (0,-1,0)<=-#macro(long)  }
rule: { (0,0,0)  } 5 {  (0,0,1)=3 and abs((0,0,0))>=abs(#macro(long)) and abs((0,-1,0))>1  }
rule: { (0,0,0) - #macro(delta)  } 5 {  (0,0,1)=3 and (0,0,0)>1 and abs((0,-1,0))<=1  }
rule: { (0,0,0) + #macro(delta)  } 5 {  (0,0,1)=3 and (0,0,0)<-1 and abs((0,-1,0))<=1  }
rule: { (0,0,0) - #macro(delta)  } 5 {  (0,0,1)=3 and (0,0,0)<-1 and (0,0,0)>-#macro(long) and (0,-1,0)<=-#macro(long)  }
rule: { (0,0,0) + #macro(delta)  } 5 {  (0,0,1)=3 and (0,0,0)>1 and (0,0,0)<#macro(long) and (0,-1,0)>=#macro(long)  }
rule: { (0,0,0) + #macro(q)*#macro(delta)  } 5 {  (0,0,1)=3 and (0,0,0)<-1 and (0,0,0)>-#macro(long) and (0,-1,0)>=#macro(long)  }
rule: { (0,0,0) - #macro(q)*#macro(delta)  } 5 {  (0,0,1)=3 and (0,0,0)>1 and (0,0,0)<#macro(long) and (0,-1,0)<=-#macro(long)  }
rule: { (0,0,0) + #macro(k)*#macro(delta)  } 5 {  (0,0,1)=3 and abs((0,0,0))<=1 and (0,-1,0)>1 and (0,-1,0)<#macro(long)  }
rule: { (0,0,0) - #macro(k)*#macro(delta)  } 5 {  (0,0,1)=3 and abs((0,0,0))<=1 and (0,-1,0)<-1 and (0,-1,0)>-#macro(long) }
rule: { (0,0,0) + #macro(q)*#macro(delta)  } 5 {  (0,0,1)=3 and abs((0,0,0))<=1 and (0,-1,0)>=#macro(long)  }
rule: { (0,0,0) - #macro(q)*#macro(delta)  } 5 {  (0,0,1)=3 and abs((0,0,0))<=1 and (0,-1,0)<-#macro(long)  }
rule: { (0,0,0)*0  } 5 {  (0,0,1)=3 and abs((0,0,0))<=#macro(delta) and abs((0,-1,0))<=1  }
rule: { (0,0,0) - #macro(delta)  } 5 {  (0,0,1)=3 and abs((0,0,0))<=1 and abs((0,-1,0))<=1 and (0,0,0)>0  }
rule: { (0,0,0) + #macro(delta)  } 5 {  (0,0,1)=3 and abs((0,0,0))<=1 and abs((0,-1,0))<=1 and (0,0,0)<0  }
rule: { (0,0,0) + #macro(delta)  } 5 {  (0,0,1)=3 and (0,0,0)>1 and (0,-1,0)>1 and (0,0,0)<(0,-1,0)  }
rule: { (0,0,0)  } 5 {  (0,0,1)=3 and (0,0,0)>1 and (0,-1,0)>1 and (0,0,0)>(0,-1,0)  }
rule: { (0,0,0) - #macro(delta)  } 5 {  (0,0,1)=3 and (0,0,0)<-1 and (0,-1,0)<-1 and (0,0,0)>(0,-1,0)  }
rule: { (0,0,0)  } 5 {  (0,0,1)=3 and (0,0,0)<-1 and (0,-1,0)<-1 and (0,0,0)<(0,-1,0)  }
rule: { if( randInt(1) < 1,(0,0,0) + #macro(delta),(0,0,0) - #macro(delta) )   } 5 {  (0,0,1)=3 and (0,0,0)*(0,-1,0)<=-1    }
rule: { (0,0,0)  } 5 {  (0,0,1)=4  and ((-1,0,0)=? or (-1,0,0)=(0,0,0)) }
rule: { (0,0,0) - #macro(delta)  } 5 {  (0,0,1)=4 and (0,0,0)<=-#macro(long) and (-1,0,0)>=#macro(long)  }
rule: { (0,0,0) + #macro(delta)  } 5 {  (0,0,1)=4 and (0,0,0)>=#macro(long) and (-1,0,0)<=-#macro(long)  }
rule: { (0,0,0)  } 5 {  (0,0,1)=4 and abs((0,0,0))>=abs(#macro(long)) and abs((-1,0,0))>1  }
rule: { (0,0,0) - #macro(delta)  } 5 {  (0,0,1)=4 and (0,0,0)>1 and abs((-1,0,0))<=1  }
rule: { (0,0,0) + #macro(delta)  } 5 {  (0,0,1)=4 and (0,0,0)<-1 and abs((-1,0,0))<=1  }
rule: { (0,0,0) - #macro(delta)  } 5 {  (0,0,1)=4 and (0,0,0)<-1 and (0,0,0)>-#macro(long) and (-1,0,0)<=-#macro(long)  }
rule: { (0,0,0) + #macro(delta)  } 5 {  (0,0,1)=4 and (0,0,0)>1 and (0,0,0)<#macro(long) and (-1,0,0)>=#macro(long)  }
rule: { (0,0,0) + #macro(q)*#macro(delta)  } 5 {  (0,0,1)=4 and (0,0,0)<-1 and (0,0,0)>-#macro(long) and (-1,0,0)>=#macro(long)  }
rule: { (0,0,0) - #macro(q)*#macro(delta)  } 5 {  (0,0,1)=4 and (0,0,0)>1 and (0,0,0)<#macro(long) and (-1,0,0)<=-#macro(long)  }
rule: { (0,0,0) + #macro(k)*#macro(delta)  } 5 {  (0,0,1)=4 and abs((0,0,0))<=1 and (-1,0,0)>1 and (-1,0,0)<#macro(long) }
rule: { (0,0,0) - #macro(k)*#macro(delta)  } 5 {  (0,0,1)=4 and abs((0,0,0))<=1 and (-1,0,0)<-1 and (-1,0,0)>-#macro(long)  }
rule: { (0,0,0) + #macro(q)*#macro(delta)  } 5 {  (0,0,1)=4 and abs((0,0,0))<=1 and (-1,0,0)>=#macro(long)  }
rule: { (0,0,0) - #macro(q)*#macro(delta)  } 5 {  (0,0,1)=4 and abs((0,0,0))<=1 and (-1,0,0)<-#macro(long)  }
rule: { (0,0,0)*0  } 5 {  (0,0,1)=4 and abs((0,0,0))<=#macro(delta) and abs((-1,0,0))<=1  }
rule: { (0,0,0) - #macro(delta)  } 5 {  (0,0,1)=4 and abs((0,0,0))<=1 and abs((-1,0,0))<=1 and (0,0,0)>0  }
rule: { (0,0,0) + #macro(delta)  } 5 {  (0,0,1)=4 and abs((0,0,0))<=1 and abs((-1,0,0))<=1 and (0,0,0)<0  }
rule: { (0,0,0) + #macro(delta)  } 5 {  (0,0,1)=4 and (0,0,0)>1 and (-1,0,0)>1 and (0,0,0)<(-1,0,0)  }
rule: { (0,0,0)  } 5 {  (0,0,1)=4 and (0,0,0)>1 and (-1,0,0)>1 and (0,0,0)>(-1,0,0)  }
rule: { (0,0,0) - #macro(delta)  } 5 {  (0,0,1)=4 and (0,0,0)<-1 and (-1,0,0)<-1 and (0,0,0)>(-1,0,0)  }
rule: { (0,0,0)  } 5 {  (0,0,1)=4 and (0,0,0)<-1 and (-1,0,0)<-1 and (0,0,0)<(-1,0,0)  }
rule: { if( randInt(1) < 1,(0,0,0) + #macro(delta),(0,0,0) - #macro(delta) )   } 5 {  (0,0,1)=4 and (0,0,0)*(-1,0,0)<=-1    }
rule: { (0,0,0) } 5 { t }
[shock-rule]
rule: { portValue(thisPort) } 5 { t }