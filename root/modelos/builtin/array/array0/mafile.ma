#include(macros.inc)

%Coupled model
[top]
components:  array1 SUM_array1_ALL_ALL0@SUM_array1_ALL_ALLDEVS_COUPLED_top SumArray10@SumArray1top var110@var11top var220@var22top  DEVS_BASIC_COUPLED_stock1  DEVS_BASIC_COUPLED_stock2 var120@var12top var210@var21top
% In ports
in: in_port_SUM_array1_ALL_ALL in_port_var12 in_port_var21
% Out ports
out: out_port_SUM_array1_ALL_ALL out_port_SumArray1 out_port_array1 out_port_flow1 out_port_flow2 out_port_stock1 out_port_stock2 out_port_var11 out_port_var22
% Input connections
link: in_port_var12 in_port_var12@var120
link: in_port_var21 in_port_var21@var210
% Output connections
link: out_port_SumArray1@SumArray10 out_port_SumArray1
link: out_port_array1@array1 out_port_array1
link: out_port_flow1@DEVS_BASIC_COUPLED_stock1 out_port_flow1
link: out_port_flow2@DEVS_BASIC_COUPLED_stock2 out_port_flow2
link: out_port_stock1@DEVS_BASIC_COUPLED_stock1 out_port_stock1
link: out_port_stock2@DEVS_BASIC_COUPLED_stock2 out_port_stock2
link: out_port_var11@var110 out_port_var11
link: out_port_var22@var220 out_port_var22
% Internal connections
link: out_port_array1@array1 in_port_array1@SUM_array1_ALL_ALL0
link: out_port_SUM_array1_ALL_ALL@SUM_array1_ALL_ALL0 in_port_SUM_array1_ALL_ALL@SumArray10
link: out_port_var11@var110 in_port_var11@array1
link: out_port_var12@var120 in_port_var12@array1
link: out_port_var21@var210 in_port_var21@array1
link: out_port_var22@var220 in_port_var22@array1
link: out_port_stock1@DEVS_BASIC_COUPLED_stock1 in_port_stock1@var110
link: out_port_stock2@DEVS_BASIC_COUPLED_stock2 in_port_stock2@var220
%Coupled model
[array1]
components: collector1@collectorarray1 array1_1_11@array1_1_1array1 array1_1_21@array1_1_2array1 array1_2_11@array1_2_1array1 array1_2_21@array1_2_2array1
% In ports
in: in_port_var11 in_port_var12 in_port_var21 in_port_var22
% Out ports
out: out_port_array1
% Input connections
link: in_port_var11 in_port_var11@array1_1_11
link: in_port_var22 in_port_var22@array1_1_11
link: in_port_var12 in_port_var12@array1_1_21
link: in_port_var11 in_port_var11@array1_2_11
link: in_port_var21 in_port_var21@array1_2_11
link: in_port_var22 in_port_var22@array1_2_11
link: in_port_var11 in_port_var11@array1_2_21
link: in_port_var22 in_port_var22@array1_2_21
% Output connections
link: out_port_array1@collector1 out_port_array1
% Internal connections
link: out_port_array1_1_1@array1_1_11 in_port_array1_1_1@collector1
link: out_port_array1_1_2@array1_1_21 in_port_array1_1_2@collector1
link: out_port_array1_2_1@array1_2_11 in_port_array1_2_1@collector1
link: out_port_array1_2_2@array1_2_21 in_port_array1_2_2@collector1
%Coupled model
[DEVS_BASIC_COUPLED_stock1]
components: flow1_stock11@flow1_stock1DEVS_BASIC_COUPLED_stock1 Totstock11@Totstock1DEVS_BASIC_COUPLED_stock1 stock11@stock1DEVS_BASIC_COUPLED_stock1
% In ports
% Out ports
out: out_port_flow1 out_port_stock1
% Input connections
% Output connections
link: out_port_flow1@flow1_stock11 out_port_flow1
link: out_port_stock1@stock11 out_port_stock1
% Internal connections
link: out_port_flow1_stock1@flow1_stock11 in_plus_port_flow1_stock1@Totstock11
link: out_port_stock1@stock11 in_port_stock1@flow1_stock11
link: out_port_Totstock1@Totstock11 in_port_Totstock1@stock11
[stock11]
% Atomic model DEVSIntegrator
x0: 10
non_negative: 0
dQMin: 0.001
dQRel: 0.001

%Coupled model
[DEVS_BASIC_COUPLED_stock2]
components: flow2_stock21@flow2_stock2DEVS_BASIC_COUPLED_stock2 Totstock21@Totstock2DEVS_BASIC_COUPLED_stock2 stock21@stock2DEVS_BASIC_COUPLED_stock2
% In ports
% Out ports
out: out_port_flow2 out_port_stock2
% Input connections
% Output connections
link: out_port_flow2@flow2_stock21 out_port_flow2
link: out_port_stock2@stock21 out_port_stock2
% Internal connections
link: out_port_flow2_stock2@flow2_stock21 in_minus_port_flow2_stock2@Totstock21
link: out_port_stock2@stock21 in_port_stock2@flow2_stock21
link: out_port_Totstock2@Totstock21 in_port_Totstock2@stock21
[stock21]
% Atomic model DEVSIntegrator
x0: 100
non_negative: 0
dQMin: 0.001
dQRel: 0.001
