#include(macros.inc)

%Coupled model
[top]
components:  DEVS_BASIC_COUPLED_stock1 converter10@converter1DEVS_COUPLED_top
% In ports
% Out ports
out: out_port_converter1 out_port_flow1 out_port_stock1
% Input connections
% Output connections
link: out_port_converter1@converter10 out_port_converter1
link: out_port_flow1@DEVS_BASIC_COUPLED_stock1 out_port_flow1
link: out_port_stock1@DEVS_BASIC_COUPLED_stock1 out_port_stock1
% Internal connections
link: out_port_converter1@converter10 in_port_converter1@DEVS_BASIC_COUPLED_stock1
link: out_port_stock1@DEVS_BASIC_COUPLED_stock1 in_port_stock1@converter10
%Coupled model
[DEVS_BASIC_COUPLED_stock1]
components: flow1_stock11@flow1_stock1DEVS_BASIC_COUPLED_stock1 Totstock11@Totstock1DEVS_BASIC_COUPLED_stock1 stock11@stock1DEVS_BASIC_COUPLED_stock1
% In ports
in: in_port_converter1
% Out ports
out: out_port_flow1 out_port_stock1
% Input connections
link: in_port_converter1 in_port_converter1@flow1_stock11
% Output connections
link: out_port_flow1@flow1_stock11 out_port_flow1
link: out_port_stock1@stock11 out_port_stock1
% Internal connections
link: out_port_flow1_stock1@flow1_stock11 in_plus_port_flow1_stock1@Totstock11
link: out_port_stock1@stock11 in_port_stock1@flow1_stock11
link: out_port_Totstock1@Totstock11 in_port_Totstock1@stock11
[stock11]
% Atomic model DEVSIntegrator
x0: 2
non_negative: 0
dQMin: 0.001
dQRel: 0.001
