#include(macros.inc)

%Coupled model
[top]
components: sumValue0@sumValuetop  DEVS_BASIC_COUPLED_stock1 converter10@converter1DEVS_COUPLED_top converter20@converter2DEVS_COUPLED_top converter30@converter3DEVS_COUPLED_top
% In ports
% Out ports
out: out_port_Flow1 out_port_converter1 out_port_converter2 out_port_converter3 out_port_stock1 out_port_sumValue
% Input connections
% Output connections
link: out_port_Flow1@DEVS_BASIC_COUPLED_stock1 out_port_Flow1
link: out_port_converter1@converter10 out_port_converter1
link: out_port_converter2@converter20 out_port_converter2
link: out_port_converter3@converter30 out_port_converter3
link: out_port_stock1@DEVS_BASIC_COUPLED_stock1 out_port_stock1
link: out_port_sumValue@sumValue0 out_port_sumValue
% Internal connections
link: out_port_converter2@converter20 in_port_converter2@DEVS_BASIC_COUPLED_stock1
link: out_port_converter1@converter10 in_port_converter1@sumValue0
link: out_port_stock1@DEVS_BASIC_COUPLED_stock1 in_port_stock1@sumValue0
%Coupled model
[DEVS_BASIC_COUPLED_stock1]
components: Flow1_stock11@Flow1_stock1DEVS_BASIC_COUPLED_stock1 Totstock11@Totstock1DEVS_BASIC_COUPLED_stock1 stock11@stock1DEVS_BASIC_COUPLED_stock1
% In ports
in: in_port_converter2
% Out ports
out: out_port_Flow1 out_port_stock1
% Input connections
link: in_port_converter2 in_port_converter2@Flow1_stock11
% Output connections
link: out_port_Flow1@Flow1_stock11 out_port_Flow1
link: out_port_stock1@stock11 out_port_stock1
% Internal connections
link: out_port_Flow1_stock1@Flow1_stock11 in_minus_port_Flow1_stock1@Totstock11
link: out_port_Totstock1@Totstock11 in_port_Totstock1@stock11
[stock11]
% Atomic model DEVSIntegrator
x0: 1000
non_negative: 0
dQMin: 0.001
dQRel: 0.001
