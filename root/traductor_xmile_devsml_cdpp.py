
import sys
import os
import shutil
cwd = os.getcwd()
if cwd not in sys.path:
    sys.path.append(cwd + '/modulosDEVS/')
    sys.path.append(cwd + '/modulosXMILE/')
from modulosDEVS.DEVSGenerator import *
import logging

###################################################################################################################
# Configuraciones
logging.basicConfig(filename='logs/traductor.log', filemode='w', level=logging.DEBUG)

###################################################################################################################

DEVSML_TEMPLATE_FILENAME = 'template-devsml.xml'
CPP_H_TEMPLATES_FILENAMES = {
    'sh': 'template-sh.sh',
    'reg': 'template-reg.cpp',
    'DEVSFtot': 'template-Ftot',
    'Fplus': 'template-Aux-Fpm',
    'Fminus': 'template-Aux-Fpm',
    'DEVSAux': 'template-Aux-Fpm',
    'DEVSConstant': 'template-Cte',
    'events': 'template-ev.ev',
    'DEVSPulse': 'template-Pulse',
    'DEVSIntegrator': 'qss1',
    'DEVSGraphical': 'template-graphical',
    'DEVSArrayCollector': 'template-array-collector',
    'DEVSArrayAgregator': 'template-array-agregator'
}

params_traducciones = {
    #'teacup': {
    #    'DIR_XMILE': 'modelos/teacup/teacup.xmile',
    #    'DEVSML_CPP_H_DIRECTORY': 'modelos/teacup/atomics/',
    #    'DEVSML_EVENTS_FILENAME': 'modelos/teacup/events.ev',
    #    'DEVSML_TOP_FILENAME': 'modelos/teacup/top.xml',
    #    'DEVSML_MA_FILENAME': 'modelos/teacup/mafile.ma',
    #    'DEVSML_SH_FILENAME': 'modelos/teacup/run.sh'
    #},
    # 'sir': {
    #    'DIR_XMILE': 'modelos/sir/sir.xmile',
    #    'DEVSML_CPP_H_DIRECTORY': 'modelos/sir/atomics/',
    #    'DEVSML_EVENTS_FILENAME': 'modelos/sir/events.ev',
    #    'DEVSML_TOP_FILENAME': 'modelos/sir/top.xml',
    #    'DEVSML_MA_FILENAME': 'modelos/sir/mafile.ma',
    #    'DEVSML_SH_FILENAME': 'modelos/sir/run.sh'
    # },
    #'lotka-volterra': {
    #    'DIR_XMILE': 'modelos/lotka-volterra/lotka-volterra.xmile',
    #    'DEVSML_CPP_H_DIRECTORY': 'modelos/lotka-volterra/atomics/',
    #    'DEVSML_EVENTS_FILENAME': 'modelos/lotka-volterra/events.ev',
    #    'DEVSML_TOP_FILENAME': 'modelos/lotka-volterra/top.xml',
    #    'DEVSML_MA_FILENAME': 'modelos/lotka-volterra/mafile.ma',
    #    'DEVSML_SH_FILENAME': 'modelos/lotka-volterra/run.sh'
    #},
    #'lotka-volterra-nested-1': {
    #    'DIR_XMILE': 'modelos/lotka-volterra-nested-1/lotka-volterra-nested-1.xmile',
    #    'DEVSML_CPP_H_DIRECTORY': 'modelos/lotka-volterra-nested-1/atomics/',
    #    'DEVSML_EVENTS_FILENAME': 'modelos/lotka-volterra-nested-1/events.ev',
    #    'DEVSML_TOP_FILENAME': 'modelos/lotka-volterra-nested-1/top.xml',
    #    'DEVSML_MA_FILENAME': 'modelos/lotka-volterra-nested-1/mafile.ma',
    #    'DEVSML_SH_FILENAME': 'modelos/lotka-volterra-nested-1/run.sh'
    #},
    #'lotka-volterra-nested-2' : {
    #    'DIR_XMILE': 'modelos/lotka-volterra-nested-2/lotka-volterra-nested-2.xmile',
    #    'DEVSML_CPP_H_DIRECTORY': 'modelos/lotka-volterra-nested-2/atomics/',
    #    'DEVSML_EVENTS_FILENAME': 'modelos/lotka-volterra-nested-2/events.ev',
    #    'DEVSML_TOP_FILENAME': 'modelos/lotka-volterra-nested-2/top.xml',
    #    'DEVSML_MA_FILENAME': 'modelos/lotka-volterra-nested-2/mafile.ma',
    #    'DEVSML_SH_FILENAME': 'modelos/lotka-volterra-nested-2/run.sh'
    #},
    # 'pulse1': {
    #     'DIR_XMILE': 'modelos/builtin/pulse1/pulse1.xmile',
    #     'DEVSML_CPP_H_DIRECTORY': 'modelos/builtin/pulse1/atomics/',
    #     'DEVSML_EVENTS_FILENAME': 'modelos/builtin/pulse1/events.ev',
    #     'DEVSML_TOP_FILENAME': 'modelos/builtin/pulse1/top.xml',
    #     'DEVSML_MA_FILENAME': 'modelos/builtin/pulse1/mafile.ma',
    #     'DEVSML_SH_FILENAME': 'modelos/builtin/pulse1/run.sh'
    # },
    # 'pulse2': {
    #     'DIR_XMILE': 'modelos/builtin/pulse2/pulse1.xmile',
    #     'DEVSML_CPP_H_DIRECTORY': 'modelos/builtin/pulse2/atomics/',
    #     'DEVSML_EVENTS_FILENAME': 'modelos/builtin/pulse2/events.ev',
    #     'DEVSML_TOP_FILENAME': 'modelos/builtin/pulse2/top.xml',
    #     'DEVSML_MA_FILENAME': 'modelos/builtin/pulse2/mafile.ma',
    #     'DEVSML_SH_FILENAME': 'modelos/builtin/pulse2/run.sh'
    # },
    # 'pulse3': {
    #     'DIR_XMILE': 'modelos/builtin/pulse3/pulse3.xmile',
    #     'DEVSML_CPP_H_DIRECTORY': 'modelos/builtin/pulse3/atomics/',
    #     'DEVSML_EVENTS_FILENAME': 'modelos/builtin/pulse3/events.ev',
    #     'DEVSML_TOP_FILENAME': 'modelos/builtin/pulse3/top.xml',
    #     'DEVSML_MA_FILENAME': 'modelos/builtin/pulse3/mafile.ma',
    #     'DEVSML_SH_FILENAME': 'modelos/builtin/pulse3/run.sh'
    # },
    # 'pulse4': {
    #     'DIR_XMILE': 'modelos/builtin/pulse4/pulse4.xmile',
    #     'DEVSML_CPP_H_DIRECTORY': 'modelos/builtin/pulse4/atomics/',
    #     'DEVSML_EVENTS_FILENAME': 'modelos/builtin/pulse4/events.ev',
    #     'DEVSML_TOP_FILENAME': 'modelos/builtin/pulse4/top.xml',
    #     'DEVSML_MA_FILENAME': 'modelos/builtin/pulse4/mafile.ma',
    #     'DEVSML_SH_FILENAME': 'modelos/builtin/pulse4/run.sh'
    # },
    # 'graphical1': {
    #     'DIR_XMILE': 'modelos/builtin/graphical1/graphical1.xmile',
    #     'DEVSML_CPP_H_DIRECTORY': 'modelos/builtin/graphical1/atomics/',
    #     'DEVSML_EVENTS_FILENAME': 'modelos/builtin/graphical1/events.ev',
    #     'DEVSML_TOP_FILENAME': 'modelos/builtin/graphical1/top.xml',
    #     'DEVSML_MA_FILENAME': 'modelos/builtin/graphical1/mafile.ma',
    #     'DEVSML_SH_FILENAME': 'modelos/builtin/graphical1/run.sh' 
    # },
    # 'graphical2': {
    #     'DIR_XMILE': 'modelos/builtin/graphical2/graphical2.xmile',
    #     'DEVSML_CPP_H_DIRECTORY': 'modelos/builtin/graphical2/atomics/',
    #     'DEVSML_EVENTS_FILENAME': 'modelos/builtin/graphical2/events.ev',
    #     'DEVSML_TOP_FILENAME': 'modelos/builtin/graphical2/top.xml',
    #     'DEVSML_MA_FILENAME': 'modelos/builtin/graphical2/mafile.ma',
    #     'DEVSML_SH_FILENAME': 'modelos/builtin/graphical2/run.sh' 
    # },
    # 'graphical3': {
    #     'DIR_XMILE': 'modelos/builtin/graphical3/graphical3.xmile',
    #     'DEVSML_CPP_H_DIRECTORY': 'modelos/builtin/graphical3/atomics/',
    #     'DEVSML_EVENTS_FILENAME': 'modelos/builtin/graphical3/events.ev',
    #     'DEVSML_TOP_FILENAME': 'modelos/builtin/graphical3/top.xml',
    #     'DEVSML_MA_FILENAME': 'modelos/builtin/graphical3/mafile.ma',
    #     'DEVSML_SH_FILENAME': 'modelos/builtin/graphical3/run.sh' 
    # },
    # 'graphical4': {
    #     'DIR_XMILE': 'modelos/builtin/graphical4/graphical4.xmile',
    #     'DEVSML_CPP_H_DIRECTORY': 'modelos/builtin/graphical4/atomics/',
    #     'DEVSML_EVENTS_FILENAME': 'modelos/builtin/graphical4/events.ev',
    #     'DEVSML_TOP_FILENAME': 'modelos/builtin/graphical4/top.xml',
    #     'DEVSML_MA_FILENAME': 'modelos/builtin/graphical4/mafile.ma',
    #     'DEVSML_SH_FILENAME': 'modelos/builtin/graphical4/run.sh'
    # },
    # 'supply-demand': {
    #     'DIR_XMILE': 'modelos/supply-demand/supply-demand.xmile',
    #     'DEVSML_CPP_H_DIRECTORY': 'modelos/supply-demand/atomics/',
    #     'DEVSML_EVENTS_FILENAME': 'modelos/supply-demand/events.ev',
    #     'DEVSML_TOP_FILENAME': 'modelos/supply-demand/top.xml',
    #     'DEVSML_MA_FILENAME': 'modelos/supply-demand/mafile.ma',
    #     'DEVSML_SH_FILENAME': 'modelos/supply-demand/run.sh'
    # },
    # 'loanable-funds': {
    #    'DIR_XMILE': 'modelos/loanable-funds-matlab/loanable-funds.xmile',
    #    'DEVSML_CPP_H_DIRECTORY': 'modelos/loanable-funds-matlab/atomics/',
    #    'DEVSML_EVENTS_FILENAME': 'modelos/loanable-funds-matlab/events.ev',
    #    'DEVSML_TOP_FILENAME': 'modelos/loanable-funds-matlab/top.xml',
    #    'DEVSML_MA_FILENAME': 'modelos/loanable-funds-matlab/mafile.ma',
    #    'DEVSML_SH_FILENAME': 'modelos/loanable-funds-matlab/run.sh'
    # },
    # 'goodwin-minsky-with-names': {
    #    'DIR_XMILE': 'modelos/goodwin-minsky-matlab/goodwin-minsky-with-names.xmile',
    #   'DEVSML_CPP_H_DIRECTORY': 'modelos/goodwin-minsky-matlab/atomics/',
    #    'DEVSML_EVENTS_FILENAME': 'modelos/goodwin-minsky-matlab/events.ev',
    #    'DEVSML_TOP_FILENAME': 'modelos/goodwin-minsky-matlab/top.xml',
    #    'DEVSML_MA_FILENAME': 'modelos/goodwin-minsky-matlab/mafile.ma',
    #     'DEVSML_SH_FILENAME': 'modelos/goodwin-minsky-matlab/run.sh'
    # },
    # 'godley-tables': {
    #     'DIR_XMILE': 'modelos/godley-tables-matlab/godley-tables-matlab.xmile',
    #     'DEVSML_CPP_H_DIRECTORY': 'modelos/godley-tables-matlab/atomics/',
    #     'DEVSML_EVENTS_FILENAME': 'modelos/godley-tables-matlab/events.ev',
    #     'DEVSML_TOP_FILENAME': 'modelos/godley-tables-matlab/top.xml',
    #     'DEVSML_MA_FILENAME': 'modelos/godley-tables-matlab/mafile.ma',
    #     'DEVSML_SH_FILENAME': 'modelos/godley-tables-matlab/run.sh'
    # },
    # 'array0': {
    #     'DIR_XMILE': 'modelos/builtin/array/array0/array0.xmile',
    #     'DEVSML_CPP_H_DIRECTORY': 'modelos/builtin/array/array0/atomics/',
    #     'DEVSML_EVENTS_FILENAME': 'modelos/builtin/array/array0/events.ev',
    #     'DEVSML_TOP_FILENAME': 'modelos/builtin/array/array0/top.xml',
    #     'DEVSML_MA_FILENAME': 'modelos/builtin/array/array0/mafile.ma',
    #     'DEVSML_SH_FILENAME': 'modelos/builtin/array/array0/run.sh'
    # },
    # 'array1': {
    #     'DIR_XMILE': 'modelos/builtin/array/array1/array1.xmile',
    #     'DEVSML_CPP_H_DIRECTORY': 'modelos/builtin/array/array1/atomics/',
    #     'DEVSML_EVENTS_FILENAME': 'modelos/builtin/array/array1/events.ev',
    #     'DEVSML_TOP_FILENAME': 'modelos/builtin/array/array1/top.xml',
    #     'DEVSML_MA_FILENAME': 'modelos/builtin/array/array1/mafile.ma',
    #     'DEVSML_SH_FILENAME': 'modelos/builtin/array/array1/run.sh'
    # },
    'array2': {
        'DIR_XMILE': 'modelos/builtin/array/array2/array2.xmile',
        'DEVSML_CPP_H_DIRECTORY': 'modelos/builtin/array/array2/atomics/',
        'DEVSML_EVENTS_FILENAME': 'modelos/builtin/array/array2/events.ev',
        'DEVSML_TOP_FILENAME': 'modelos/builtin/array/array2/top.xml',
        'DEVSML_MA_FILENAME': 'modelos/builtin/array/array2/mafile.ma',
        'DEVSML_SH_FILENAME': 'modelos/builtin/array/array2/run.sh'
    },
    'array3': {
        'DIR_XMILE': 'modelos/builtin/array/array3/array3.xmile',
        'DEVSML_CPP_H_DIRECTORY': 'modelos/builtin/array/array3/atomics/',
        'DEVSML_EVENTS_FILENAME': 'modelos/builtin/array/array3/events.ev',
        'DEVSML_TOP_FILENAME': 'modelos/builtin/array/array3/top.xml',
        'DEVSML_MA_FILENAME': 'modelos/builtin/array/array3/mafile.ma',
        'DEVSML_SH_FILENAME': 'modelos/builtin/array/array3/run.sh'
    }
}

for model, params in params_traducciones.items():
    DIR_XMILE = params['DIR_XMILE']
    DEVSML_CPP_H_DIRECTORY = params['DEVSML_CPP_H_DIRECTORY']
    DEVSML_TOP_FILENAME = params['DEVSML_TOP_FILENAME']
    DEVSML_EVENTS_FILENAME = params['DEVSML_EVENTS_FILENAME']
    DEVSML_MA_FILENAME = params['DEVSML_MA_FILENAME']
    DEVSML_SH_FILENAME = params['DEVSML_SH_FILENAME']

    try:
        shutil.rmtree(DEVSML_CPP_H_DIRECTORY)
        os.makedirs(DEVSML_CPP_H_DIRECTORY)
    except Exception:
        os.makedirs(DEVSML_CPP_H_DIRECTORY)
    # Initialize directory
    shutil.copyfile('templates/Makefile', DEVSML_CPP_H_DIRECTORY + '/Makefile')
    shutil.copyfile('templates/macros.inc', DEVSML_CPP_H_DIRECTORY + '/macros.inc')
    shutil.copyfile('templates/tuple_to_real.h', DEVSML_CPP_H_DIRECTORY + '/tuple_to_real.h')
    shutil.copyfile('templates/tuple_to_real.cpp', DEVSML_CPP_H_DIRECTORY + '/tuple_to_real.cpp')
    shutil.copyfile('templates/equation_calculator.h', DEVSML_CPP_H_DIRECTORY + '/equation_calculator.h')
    shutil.copyfile('templates/equation_calculator.cpp', DEVSML_CPP_H_DIRECTORY + '/equation_calculator.cpp')

    # Generate .devsml file
    generateDEVSML(DIR_XMILE, DEVSML_TEMPLATE_FILENAME, DEVSML_TOP_FILENAME)

    # Generate .ma, .ev, .cpp, .h, reg.cpp 
    generateHCPP(DEVSML_TOP_FILENAME, DEVSML_CPP_H_DIRECTORY, CPP_H_TEMPLATES_FILENAMES, DEVSML_EVENTS_FILENAME,
                 DEVSML_MA_FILENAME)

    # Generate run.sh
    # TODO
    #