
# TODO : hay que importar los modulos correspondientes a todas las SpecialFunctions
from modulosDEVS.DEVSAtomic.DEVSPulse import DEVSPulse
from modulosDEVS.DEVSAtomic.DEVSArrayAgregator import DEVSArrayAgregator
import re

class SpecialFunctionFinder(object):
    def __init__(self):
        # TODO: sacar estas regex a un archivo
        self.regexs = {
            'PULSE': [
                (r"PULSE\([ ]*[\w]+[ ]*\)", r"PULSE\([ ]*([\w]+)[ ]*\)"),
                (r"PULSE\([ ]*[\w]+[ ]*,[ ]*[\w]+[ ]*\)", r"PULSE\([ ]*([\w]+)[ ]*,[ ]*([\w]+)[ ]*\)"),
                (r"PULSE\([ ]*[\w]+[ ]*,[ ]*[\w]+[ ]*,[ ]*[\w]+[ ]*\)", r"PULSE\(([\w]+),([\w]+),([\w]+)\)")
            ],
            'RAMP' : [
               (r"RAMP\([ ]*[\w]+[ ]*\)", r"RAMP\([ ]*([\w]+)[ ]*\)"),
               (r"RAMP\([ ]*[\w]+[ ]*,[ ]*[\w]+[ ]*\)", r"RAMP\([ ]*([\w]+)[ ]*,[ ]*([\w]+)[ ]*\)")
            ],
            'STEP': [
                (r"STEP\([ ]*[\w]+[ ]*\)", r"STEP\([ ]*([\w]+)[ ]*\)"),
                (r"STEP\([ ]*[\w]+[ ]*,[ ]*[\w]+[ ]*\)", r"STEP\([ ]*([\w]+)[ ]*,[ ]*([\w]+)[ ]*\)")
            ]
        }
        assert(set(self.regexs.keys()) == set(self.regexs.keys()))

        self.agregation_functions = ['SUM', 'PROD', 'MEAN']

    def getSpecialFunctionsNames(self):
        return self.regexs.keys()

    # TODO: REFACTORIZAR
    #####################################################################
    def parseEquation(self, equation, sim_specs, dimensions, destiny_name):
        special_functions = []
        array_functions = []
        
        equation = equation.replace(" ", "")

        if '[' in equation and ']' in equation:
            # Parseo funcion de agregacion sobre arrays
            array_functions_with_parameters = self.getArrayFunctions(equation)
            for array_function in array_functions_with_parameters:
                obj_func = self.parseArrayFunction(destiny_name, array_function, sim_specs, dimensions)
                array_functions.append(obj_func)
                equation = equation.replace(array_function, obj_func.get_name())

        # Parseo funciones especiales (PULSE, RAMP, STEP)
        special_functions_with_parameters = self.getSpecialFunctionsWithParameters(equation)
        for func_with_params in special_functions_with_parameters:
            obj_func = self.parseSpecialFunctionWithParameters(destiny_name, func_with_params, sim_specs)
            special_functions.append(obj_func)
            equation = equation.replace(func_with_params, obj_func.get_name())
        
        # Devuelvo todas las funciones (especiales + arrayed)
        return special_functions + array_functions, equation
    
    #####################################################################
    # ArrayFunctions
    def getArrayFunctions(self, equation):
        if "*" in equation:
            for ag_f in self.agregation_functions:
                regex = ag_f + "\(" + "[\w]+\[[\*\d+][,\*\d+]*\]" + "\)"
                search_results = re.findall(re.compile(regex), equation)
                if len(search_results) > 0:
                    break
        else:
            regex = r"[\w]+\[[\*\d+][,\*\d+]*\]"
            search_results = re.findall(re.compile(regex), equation)
        return search_results

    def parseArrayFunction(self, destiny_name, array_function, sim_specs, dimensions):

        return DEVSArrayAgregator(destiny_name, dimensions, array_function)

    #####################################################################
    # SpecialFunctionsWithParameters
    def getSpecialFunctionsWithParameters(self, equation):
        ans = []
        for func_name in self.regexs.keys():
            regexs = self.regexs[func_name]
            for regex in regexs:
                search_results = re.findall(re.compile(regex[0]), equation)
                ans = ans + search_results
        return ans
    def parseSpecialFunctionWithParameters(self, destiny_name, func_with_parameters, sim_specs):
        # Chequeo que la funcion este entre las que puedo parsear
        func_name = None
        ok = False
        for name in self.regexs.keys():
            if name in func_with_parameters:
                ok = True
                func_name = name
        assert(ok)
        assert(func_name is not None)
        return self.generateSpecialFunction(destiny_name, func_name, func_with_parameters, sim_specs)

    #####################################################################
    # Funcion auxiliar
    # SpecialFunctionGenerator
    def generateSpecialFunction(self, destiny_name, func_name, func_with_parameters, sim_specs):
        #######################
        # IMPORTANTE : 'destiny_name'. PASAR EL NOMBRE DEL PADRE (EN DONDE ESTA DEFINIDA LA FUNCION, PARA QUE SEA INCLUIDO EN EL NOMBRE)
        #######################
        # PULSE(volume, [<first pulse>, <interval>])
        if func_name == 'PULSE':
            devs_pulse = None
            regexs_pulse = self.regexs['PULSE']
            for regex in regexs_pulse:
                func_with_parameters = func_with_parameters.replace(' ', '')
                search_res = re.search(regex[1], func_with_parameters)
                if search_res is not None:
                    parameters = list(search_res.groups())  
                    if len(parameters) < 3:
                        raise Exception('Error : parametros incorrectos en funcion PULSE')
                    volume = parameters[0]
                    if len(parameters) > 1:
                        first_pulse = parameters[1]
                    if len(parameters) > 2:
                        interval = parameters[2]
                        dt = sim_specs['dt']
                        devs_pulse = DEVSPulse(destiny_name, volume, first_pulse, interval, dt)
            assert(devs_pulse is not None)
            return devs_pulse
        else:
            raise Exception('Error : la funcion ' + func_name + ' no puede ser generada')